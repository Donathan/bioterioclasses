/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author donathan
 */
public class Estante {

    private int idEstante;
    private final List<Prateleira> prateleiras = new ArrayList<>();

    //teste
    private static int contador = 0;

    public Estante(int qntPrateleira, int tamanhoPrateleira) {
        criarPrateleira(qntPrateleira, tamanhoPrateleira);

        //teste
        idEstante = ++contador;

    }

    private void criarPrateleira(int qntPrateleira, int tamanhoPrateleira) {
        for (int i = 0; i < qntPrateleira; i++) {
            addPrateleira(new Prateleira(tamanhoPrateleira));
        }
    }

    private void addPrateleira(Prateleira prateleira) {
        prateleiras.add(prateleira);
        prateleira.setEstante(this);

    }

    @Override
    public String toString() {
        return "ESTANTE " + getIdEstante();
    }

    public int getPrateleira(Prateleira p) {
        return prateleiras.indexOf(p);

    }

    public int getIdEstante() {
        return idEstante;
    }

    public void setIdEstante(int idEstante) {
        this.idEstante = idEstante;
    }

    public Prateleira getPrateleira(int i) {
        return prateleiras.get(i);
    }

    public List<Prateleira> getListPrateleira() {
        return prateleiras;
    }

    public int getQuantidadePrateleiras() {
        return prateleiras.size();
    }

}
