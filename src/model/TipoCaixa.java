/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author donathan
 */
public enum TipoCaixa {
    ESTOQUE(0), EXPERIMENTO(1), BERCARIO(2);

    private final int idTipoCaixa;

    private TipoCaixa(int a) {
        this.idTipoCaixa = a;
        return;
    }

    public int getIdTipoCaixa() {
        return idTipoCaixa;
    }
    
    

}
